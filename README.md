# Tictactoe

Implémentation C++/JavaScript du jeu de Tictactoe.


- Nom: SCHERIER
- Prénom: REMY
- Groupe: APP


## Compilation du projet C++

Projet CMake classique, dans le dossier `cpp` :

```
nix-shell
mkdir build
cmake ..
make
...
```

## Compilation du projet JavaScript

Dans le dossier `js` :


```
nix-shell
make
make run
...
```


