#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP

#include <array>
#include <iostream>

enum Joueur {
    JOUEUR_VIDE, JOUEUR_ROUGE, JOUEUR_VERT, JOUEUR_EGALITE
};

class Jeu {
private:
    std::string grille[3][3] = {
        {".", ".", "."},
        {".", ".", "."},
        {".", ".", "."}
        };
        
    int Tour = 0;

public:
    // \brief Constructeur à utiliser.
    Jeu();

    // \brief Recupère le vainqueur (R, V, EGALITE) ou VIDE si partie en cours.
    Joueur getVainqueur() const;

    // \brief Retourne si victoire verticale
    bool winVerticalement() const;

    // \brief Retourne si victoire horizontale
    bool winHorizontal() const;

    // \brief Retourne si victoire diagonale
    bool winDiagonale() const;

    // \brief Retourne si le joueur à gagné
    bool hasWin() const;

    // \brief Recupère le joueur qui doit jouer. (R, V)
    Joueur getJoueurCourant() const;

    // \brief récupérer la couleur du joueur.
    std::string getCouleurJoueurCourant() const;

    // \brief Joue un coup
    //
    // \param x ligne  (0 1 2)
    // \param y colonne  (0 1 2)
    //
    // \brief Retourne si le coup est valide. Faux si le coup n'est pas valide,
    // \brief joue le coup.
    bool jouer(int x, int y);

    // \brief Réinitialise le jeu.
    void raz();

    // \brief Corps du jeu
    void main();


    friend std::ostream &operator<<(std::ostream &os, const Jeu &jeu);
};


std::ostream &operator<<(std::ostream &os, const Jeu &jeu);

#endif

