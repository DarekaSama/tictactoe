#include "Tictactoe.hpp"
#include <sstream>
#include "catch.hpp"

bool checkStream(Jeu &jeu,std::string string){
    std::stringstream os;
    os << jeu;
    return os.str() == string;
}
TEST_CASE("test bidon") { 
    REQUIRE( true );
}

TEST_CASE("test affichage") {
    Jeu jeu;
    REQUIRE(checkStream(jeu,"...\n...\n...\n"));
}

TEST_CASE("test jouer"){
    Jeu jeu;
    REQUIRE(jeu.jouer(1,1));
    REQUIRE(!jeu.jouer(1,1));
}

TEST_CASE("vertical"){
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(1,1);
    jeu.jouer(1,0);

    jeu.jouer(1,2);
    jeu.jouer(2,0);
    REQUIRE(jeu.winVerticalement());
}

TEST_CASE("horizontal"){
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(1,1);
    jeu.jouer(0,1);

    jeu.jouer(1,2);
    jeu.jouer(0,2);
    REQUIRE(jeu.winHorizontal());
}
TEST_CASE("diagonale1"){
    Jeu jeu;
    jeu.jouer(0,0);
    jeu.jouer(2,1);
    jeu.jouer(1,1);

    jeu.jouer(1,2);
    jeu.jouer(2,2);
    REQUIRE(jeu.winDiagonale());
}

TEST_CASE("diagonale2"){
    Jeu jeu;
    jeu.jouer(0,2);
    jeu.jouer(2,1);
    jeu.jouer(1,1);

    jeu.jouer(1,2);
    jeu.jouer(2,0);
    REQUIRE(jeu.winDiagonale());
}

TEST_CASE("egalite"){
    Jeu jeu;
    jeu.jouer(0,2);
    jeu.jouer(1,2);
    jeu.jouer(2,2);

    jeu.jouer(0,1);
    jeu.jouer(2,1);
    jeu.jouer(1,1);

    jeu.jouer(1,2);
    jeu.jouer(1,0);
    jeu.jouer(2,0);

    jeu.jouer(0,0);
    REQUIRE(jeu.getVainqueur() == JOUEUR_EGALITE);

}






