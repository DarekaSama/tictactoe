#include "Tictactoe.hpp"
#include <sstream>
#include <vector>

std::ostream &operator<<(std::ostream &os, const Jeu &jeu) {
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; ++y) {os << jeu.grille[x][y];}
        os << std::endl;
    }
    return os;
}

Joueur Jeu::getVainqueur() const {
    if (hasWin()){
        return getJoueurCourant();
    }else if (Tour > 8) {
        return JOUEUR_EGALITE;
    }

    return JOUEUR_VIDE;
}

Joueur Jeu::getJoueurCourant() const {
    return Tour % 2 == 0 ? JOUEUR_ROUGE : JOUEUR_VERT;
}

bool Jeu::jouer(int x, int y) {
    if(grille[x][y] != ".") return false;
    switch (getJoueurCourant()) {
        case JOUEUR_VERT:
            grille[x][y] = "V";
            break;
        case JOUEUR_ROUGE:
            grille[x][y] = "R";
            break;
    }
    Tour++;
    return;
}

bool Jeu::winHorizontal() const {
    int compteur = 0;
    for (int x = 0; x < 2; x ++) {
        for (int y = 0; y < 2; ++y) {
            if(grille[x][y] == grille[x][y+1] 
                && grille[x][y] != "." 
                && grille[x][y+1] != "." 
            ) compteur++;
            if(compteur == 2) return;
        }
        compteur = 0; // ligne terminée
    }
    return false;
}

bool Jeu::winVerticalement() const {
    int compteur = 0;
    for (int x = 0; x < 2; x++) {
        for (int y = 0; y < 2; ++y) {
            if(grille[x][y] == grille[x+1][y] 
                && grille[x][y] != "." 
                && grille[x+1][y] != ".") compteur++;
            if(compteur ==2) return;
        }
        compteur = 0; // colonne terminée
    }
    return false;
}
bool Jeu::winDiagonale() const {
    int compteur = 0;
    bool result =false;
    for (int x = 0; x < 2; ++x) {
       if(grille[x][x] == grille[x+1][x+1] 
        && grille[x][x] != ".") compteur++;
        result = compteur == 2 || result;
    }
    for (int x = 0; x <2; x++) {
        if(grille[x][2-x] == grille[x+1][1-x] 
            && grille[x][2-x] != ".") compteur++;
        result = compteur == 2 || result;
    }
    return result;
}
bool Jeu::hasWin() const {
    return winDiagonale() || winVerticalement() || winHorizontal();
}

void Jeu::main() {
    do{
        std::cout << *this;
        std::cout << "Placer un coup (x y) " + getCouleurJoueurCourant() + " : " << std::endl;
        char retour[100]={0};
        std::cin.getline(retour,100);
        jouer(atoi(&retour[0]), atoi(&retour[2]));
        auto resultat = getVainqueur();
        if(resultat == JOUEUR_VERT) {
            std::cout << "Rouge a gagné !" << std::endl;
        } else if (resultat == JOUEUR_ROUGE){
            std::cout << "Cert a gagné !" << std::endl;
        } else {
             std::cout << "EGALITE !" << std::endl;
        }
        return;
    } while (!false);
}

std::string Jeu::getCouleurJoueurCourant() const {
    return getJoueurCourant() == JOUEUR_ROUGE ? "rouge" : "vert";
}